package com.example.myapplication

import android.app.*
import android.content.*
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.myapplication.Utils.COUNTDOWN_BR
import com.example.myapplication.Utils.actionOpenActivity
import com.example.myapplication.Utils.registerBroadcast
import com.example.myapplication.Utils.unregisterBroadcast
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit


class MyService : Service() {
    val NOTIF_ID = 1
    val NOTIF_CHANNEL_ID = "CHANNEL_ID"
    lateinit var job: Job
    val timeReStream: Long = 10 * 1000

    var check = true
    lateinit var broadcastReceiver: MyReciever

    lateinit var countDownTimer: CountDownTimer
    lateinit var sharedPreferences: SharedPreferences

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startForeroundService()
        else
            startForeground(1, Notification())

        check = true

        broadcastReceiver = MyReciever()
        if (check == true)
            registerBroadcast(broadcastReceiver)
        //  initialJob()


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            resetWhenCheckPermisison()
//        }
        return START_STICKY

    }


    fun startForeroundService() {

        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service", NOTIF_CHANNEL_ID)
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }
        startForeground(
            NOTIF_ID, NotificationCompat.Builder(
                this,
                channelId
            )
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Service is running background")
                .build()
        )


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG2", "Service onDestroy: ")
        sendBroadcast(Intent(actionOpenActivity))
    }


    override fun onTaskRemoved(rootIntent: Intent?) {
        Log.d("APPREMOVE", "APP IS REMOVED")

        // app is killed
        Intent(this, MainActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(this)
        }

//      applicationContext.sendBroadcast(Intent(actionOpenActivity).addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES))

    }

    /** open another app*/
    private fun initialJob() {
        job = CoroutineScope(Dispatchers.IO).launch {
            while (true) {
//                 if (!isAppRunning(this@MyService, "com.example.myapplication"))
//                    Intent(this@MyService, MainActivity::class.java).apply {
//                        this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(this)
//                    }

                startNewActivity(this@MyService, "com.example.application2")  // to open app

                startNewService() // to open service
                Log.d("DEBUGAPP", " initialJob1 ")

                delay(timeReStream)

            }
        }


    }

    private fun cancelJob() {
        job.cancel()
        Log.e("TAG", "Job is Cancel")
    }

    fun startNewActivity(context: Context, packageName: String) {
        var intent: Intent = context.getPackageManager().getLaunchIntentForPackage(packageName)!!
        if (intent != null) {
            // We found the activity now start the activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            intent.addFlags(Intent.FLAG_FROM_BACKGROUND)
            context.startActivity(intent)

        } else {
            // Bring user to the market or let them choose an app?
            intent = Intent(Intent.ACTION_VIEW)
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK )

            intent.data = Uri.parse("market://details?id=$packageName")
            context.startActivity(intent)
        }

//        val intent = Intent(Intent.ACTION_MAIN)
//            .addCategory(Intent.CATEGORY_LAUNCHER)
//            .setClassName("com.example.application2", "com.example.application2.MainActivity")
//            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            .addFlags(Intent.FLAG_FROM_BACKGROUND)
//            .setComponent(ComponentName("com.example.application2", "com.example.application2.MainActivity"))
//        applicationContext.startActivity(intent)


    }


    fun startNewService() {
        val intent = Intent()
//        intent.setClassName("com.example.application2","com.example.application2.MyService")

        intent.component =
            ComponentName("com.example.application2", "com.example.application2.MyService")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)

        }

    }


    inner class MyReciever : BroadcastReceiver() {

        override fun onReceive(p0: Context?, p1: Intent?) {

//        if (p1?.action == actionOpenActivity) {
//            Intent(p0, MainActivity::class.java).apply {
//                this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                p0!!.startActivity(this)
//            }
//        }
//
//        if (p1?.action == actionOpenService) {
//
//            Intent(p0, MyService::class.java).apply {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    startForegroundService(p0!!, this)
//                } else p0?.startService(this)
//            }
//        }


            try {

                if (p1?.action == Utils.actionEnableWifi) {
                    check = false
                    unregisterBroadcast(broadcastReceiver)
                    initCountdown()

                }

                if (checkNetWorkConnection(p0!!)) {
                    disableWifiConnection(p0)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun disableWifiConnection(context: Context) {
            val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager

            val wifiEnabled = wifiManager.isWifiEnabled()

            if (wifiEnabled == true) wifiManager.isWifiEnabled = false

            Toast.makeText(context, "Wifi is disable", Toast.LENGTH_SHORT).show()

            Log.d("TAG", wifiEnabled.toString())


        }

        private fun checkNetWorkConnection(context: Context): Boolean {

            var connect =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var connectInfo = connect.activeNetworkInfo

            if (connectInfo != null && connectInfo.isConnected) {
                return connectInfo?.type == ConnectivityManager.TYPE_WIFI

            }
            return false
        }
    }


    fun initCountdown() {
        val intent = Intent(COUNTDOWN_BR)

        sharedPreferences = getSharedPreferences(packageName, MODE_PRIVATE)
        var millis: Long = sharedPreferences.getLong("time", TimeUnit.MINUTES.toMillis(59))


        if (millis / 1000 == 0L) {
            millis = TimeUnit.MINUTES.toMillis(59)

        }
        countDownTimer = object : CountDownTimer(millis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                intent.putExtra("countdown", millisUntilFinished)
                sendBroadcast(intent)
            }

            override fun onFinish() {
                intent.putExtra("countdown", 0)
                check = true
                registerBroadcast(broadcastReceiver)
            }
        }
        countDownTimer.start()
    }
}