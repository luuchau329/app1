package com.example.myapplication

import android.app.ActivityManager
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener

object Utils {
    val actionOpenActivity = "com.example.myapplication.RestartActivity"
    val actionOpenService = "com.example.myapplication.RestartService"
    val actionEnableWifi = "com.example.myapplication.EnableWifi"
    val COUNTDOWN_BR = "com.example.myapplication.backgroundtimerout"

    fun Context.registerBroadcast(reciever: MyService.MyReciever){
        var intentFilter = IntentFilter()
            intentFilter.addAction(actionOpenActivity)
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            intentFilter.addAction(actionOpenService)
            intentFilter.addAction(actionEnableWifi)
            this.registerReceiver(reciever, intentFilter)
    }

    fun Context.unregisterBroadcast(reciever: MyService.MyReciever){
        this.unregisterReceiver(reciever)
    }

    fun isAppRunning(context: Context, packageName: String): Boolean {
        val activityManager = context.getSystemService(AppCompatActivity.ACTIVITY_SERVICE) as ActivityManager
        val procInfos = activityManager.runningAppProcesses
        if (procInfos != null) {
            for (processInfo in procInfos) {
                if (processInfo.processName == packageName) {
                    return true
                }
            }
        }
        return false
    }












//    private fun checkPermission() {
//        val allPermissionsListener: MultiplePermissionsListener =
//            CompositeMultiplePermissionsListener(
//                object : MultiplePermissionsListener {
//                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
//                        if (report.deniedPermissionResponses.size <= 0) {
//                            //TODO Làm gì khi có quyền ở đây
//                            Toast.makeText(this@MainActivity, "fdgdfgfd", Toast.LENGTH_SHORT).show()
//                        }
//                    }
//
//                    override fun onPermissionRationaleShouldBeShown(
//                        permissions: List<PermissionRequest?>?,
//                        token: PermissionToken?
//                    ) {
//                    }
//                }, SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(
//
//                    layoutMain,
//                    R.string.all_permissions_denied_feedback
//                )
//                    .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
//                    .build()
//            )
//        val errorListener =
//            PermissionRequestErrorListener { error ->
//                Log.e(
//                    "DEBUGAPP",
//                    "There was an storeError: $error"
//                )
//            }
//        Dexter.withContext(this)
//            .withPermissions("android.permission.READ_EXTERNAL_STORAGE") //TODO Thêm quyền cần check chỗ này
//            .withListener(allPermissionsListener)
//            .withErrorListener(errorListener)
//            .check()
//    }
}