package com.example.myapplication

import android.app.ActivityManager
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.example.myapplication.Utils.COUNTDOWN_BR
import com.example.myapplication.Utils.actionEnableWifi
import java.text.DecimalFormat


class MainActivity : AppCompatActivity() {

    private var wakeLock: PowerManager.WakeLock? = null

    lateinit var btnWifiEnable: AppCompatButton
    lateinit var wifi: WifiManager
    lateinit var txtWifiDisable: TextView
    lateinit var txtTimeRemain: TextView
    lateinit var llWifiEnalbe: LinearLayout


    companion object {
        var STATEBUTTON = 1 // state = 1 -> turn on wifi , state = 2 -> turn off wifi
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    Log.d("DEBUGAPP", "APP1 PERMISSION ACCESS")
                } else {
                    // Permission Granted-System will work
                    Log.d("DEBUGAPP", "APP1 PERMISSION DENNY")
                    resetResultLauncher()

                }
            }
        }

    fun resetResultLauncher() {
        var intent = Intent(
            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:" + getPackageName())
        )
        resultLauncher.launch(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        setOnClick()

        wifi = applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
        startMyService()
        addOverlay()

//        broadcastReceiver = MyReciever()

        // Declaring Wi-Fi manager
        // On button Click

//        registerBroadcast(broadcastReceiver)

//        btn.setOnClickListener {
//
//            // wifi.isWifiEnabled is a boolean, is Wi-Fi is ON,
//            // it switches down and vice-versa
//            wifi.setWifiEnabled(!wifi.isWifiEnabled)
//
//            // For displaying Wi-fi status in TextView
//            if (!wifi.isWifiEnabled) {
//                textView.text = "Wifi is ON"
//            } else {
//                textView.text = "Wifi is OFF"
//            }
//        }

        Log.d("DEBUGAPP", "App1 onCreate: ")

    }

    private fun initView() {
        btnWifiEnable = findViewById(R.id.btnSetWifi)
        txtWifiDisable = findViewById(R.id.txtWifiDisable)
        txtTimeRemain = findViewById(R.id.txtTimeRemain)
        llWifiEnalbe = findViewById(R.id.llWifiEnalbe)
    }

    private fun setOnClick() {

        btnWifiEnable.setOnClickListener {
            when (STATEBUTTON) {
                1 -> showLoginDialog(this)
                2 -> showDisableWifiDialog(this)

            }

        }
    }

    private var countDownBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //Update GUI
            updateGUI(intent)
        }
    }

    private fun showLoginDialog(context: Context) {

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val viewGroup = findViewById<ViewGroup>(androidx.appcompat.R.id.content)
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.dialog_login, viewGroup, false)
        builder.setView(dialogView)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()

        val edtID = dialogView.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.edtID)
        val edtPW = dialogView.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.edtPW)
        val txtError = dialogView.findViewById<TextView>(R.id.txtError)

        dialogView.findViewById<AppCompatButton>(R.id.btnCancle).setOnClickListener {
            alertDialog.dismiss()
        }

        dialogView.findViewById<AppCompatButton>(R.id.btnUsingWifi).setOnClickListener {
            if (edtID.text.toString() == "1" && edtPW.text.toString() == "1") {

                txtError.visibility = View.INVISIBLE
                alertDialog.dismiss()
                sendBroadcast(Intent(actionEnableWifi))
                wifi.setWifiEnabled(true)
                STATEBUTTON = 2


                txtWifiDisable.visibility = View.GONE
                llWifiEnalbe.visibility = View.VISIBLE
                btnWifiEnable.text = "禁止"
            } else {
                txtError.visibility = View.VISIBLE
            }

        }

    }

    private fun showDisableWifiDialog(context: Context) {

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val viewGroup = findViewById<ViewGroup>(androidx.appcompat.R.id.content)
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.dialog_notifi, viewGroup, false)
        builder.setView(dialogView)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()



        dialogView.findViewById<AppCompatButton>(R.id.btnCancle).setOnClickListener {
            alertDialog.dismiss()
        }

        dialogView.findViewById<AppCompatButton>(R.id.btnOK).setOnClickListener {
            STATEBUTTON = 1
            btnWifiEnable.text = "Wifi利用"
            txtWifiDisable.visibility = View.VISIBLE
            llWifiEnalbe.visibility = View.GONE
            btnWifiEnable.text = "Wifi利用"
            alertDialog.dismiss()

            startMyService()

        }

    }


    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }


    fun startMyService() {

        wakeLock =
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyService::lock").apply {
                    acquire()
                }
            }

        Intent(this, MyService::class.java).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(this)
            } else startService(this)
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(countDownBroadcastReceiver, IntentFilter(COUNTDOWN_BR));

    }

    override fun onDestroy() {
        super.onDestroy()

        if (!isMyServiceRunning(MyService::class.java))
            startMyService()
//        if (hasRegisterBroadcast)
//            unregisterBroadcast(broadcastReceiver)
    }

    override fun onStop() {
        super.onStop()
        Log.d("DEBUGAPP", "App1 onStop: ")
        try {
            unregisterReceiver(countDownBroadcastReceiver)
        } catch (e: Exception) {
            // Receiver was probably already
        }
    }


    fun addOverlay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                var intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName())
                )

                resultLauncher.launch(intent)
            }
        }
    }


    private fun updateGUI(intent: Intent) {
        if (intent.extras != null) {


            var millisUntilFinished = intent.getLongExtra("countdown", 30000)

            if (millisUntilFinished == 0L) {
                txtWifiDisable.visibility = View.VISIBLE
                llWifiEnalbe.visibility = View.GONE

                btnWifiEnable.text = "Wifi利用"

            }

            var f = DecimalFormat("00");


            var min = (millisUntilFinished / 60000) % 60

            var sec = (millisUntilFinished / 1000) % 60

            txtTimeRemain.setText("残り時間: " + f.format(min) + ":" + f.format(sec));
//            txtTimeRemain.setText((time).toString())
            val sharedPreferences = getSharedPreferences(packageName, MODE_PRIVATE)
            sharedPreferences.edit().putLong("time", millisUntilFinished).apply()
        }
    }


}

